<?php

use App\Http\Controllers\CarController;
use App\Http\Controllers\UserCarController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// user.car
Route::get('user/{userId}/cars', [UserCarController::class, 'getList']);
Route::get('user/{userId}/cars/undelivered', [UserCarController::class, 'getUnDeliveredList']); //route格式錯誤,僅顯示用

// car
Route::get('cars', [CarController::class, 'getList']);
