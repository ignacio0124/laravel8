<?php

namespace App\Models;

use Kra8\Snowflake\HasSnowflakePrimary;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserCar extends Model
{
    use HasSnowflakePrimary, HasFactory;

    protected $casts = [
        'id'                 => 'string',
        'user_id'            => 'string',
        'car_information_id' => 'string',
        'is_delivered'       => 'boolean',
    ];

    /**
     * 與 cars 關聯
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }

    /**
     * scope a query 未交車的車
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeUnDelivered($query)
    {
        return $query->where('is_delivered', '=', false);
    }
}
