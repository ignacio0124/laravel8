<?php

namespace App\Models;

use Kra8\Snowflake\HasSnowflakePrimary;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Car extends Model
{
    use HasSnowflakePrimary, HasFactory;

    const COLOR_WHITE = 0;
    const COLOR_BLUE  = 1;
    const COLOR_RED   = 2;
    const COLOR_BLACK = 3;
    const COLOR_WORDING_ARRAY = [
        self::COLOR_WHITE => 'White',
        self::COLOR_BLUE  => 'Blue',
        self::COLOR_RED   => 'Red',
        self::COLOR_BLACK => 'Black',
    ];

    /**
     * 取得color文字
     *
     * @return string
     */
    public function getColorTextAttribute()
    {
        return self::COLOR_WORDING_ARRAY[$this->color];
    }
}
