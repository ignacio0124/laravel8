<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\Models\UserCar;

class UserCarRepository extends AbstractRepository
{
    public function __construct(UserCar $car)
    {
        parent::__construct($car);
    }

    /**
     * 新增一筆資料
     *
     * @param array $data
     */
    public function create($data)
    {
        $car = $this->getNew();

        $car->user_id            = $data['user_id'];
        $car->car_information_id = $data['car_id'];
        $car->is_delivered       = $data['is_delivered'];

        $car->save();
    }

    /**
     * 更新交車狀態
     *
     * @param int   $id
     * @param array $data
     */
    public function updateDeliver($id, $data)
    {
        $car = $this->getById($id);

        $car->is_delivered = $data['is_delivered'];

        $car->save();
    }

    /**
     * 取得客戶所有的車
     *
     * @param int $userId
     * @return Collection
     */
    public function getAllByUserId($userId)
    {
        return $this->model
            ->with('car')
            ->where('user_id', '=', $userId)
            ->get();
    }

    /**
     * 取得未交付的車
     *
     * @param int $userId
     * @return Collection
     */
    public function getUnDeliveredByUserId($userId)
    {
        return $this->model
            ->with('car')
            ->unDelivered()
            ->where('user_id', '=', $userId)
            ->get();
    }
}
