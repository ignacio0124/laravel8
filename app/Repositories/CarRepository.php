<?php

namespace App\Repositories;

use App\Models\Car;
use Illuminate\Database\Eloquent\Collection;

class CarRepository extends AbstractRepository
{
    public function __construct(Car $carInformation)
    {
        parent::__construct($carInformation);
    }

    /**
     * 取得符合車名的車
     *
     * @param string $search
     * @return Collection
     */
    public function searchByName($search)
    {
        return $this->model
            ->where('name', 'like', '%' . $search . '%')
            ->get();
    }
}
