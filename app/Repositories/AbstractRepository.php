<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class AbstractRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * 取得新model
     *
     * @return Model
     */
    public function getNew()
    {
        return $this->model->newInstance();
    }

    /**
     * 根據id取得model
     *
     * @param int $id
     * @return Model
     */
    public function getById($id)
    {
        return $this->model->where('id', '=', $id)->first();
    }

    /**
     * 判斷是否存在
     *
     * @param int $id
     * @return boolean
     */
    public function exists($id)
    {
        return $this->model->where('id', '=', $id)->exists();
    }
}
