<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCarListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->collection->map(function ($item) {
            return [
                'color'        => $item->car->color_text,
                'displacement' => $item->car->displacement,
                'price'        => $item->car->price,
                'isDelivered'  => $item->is_delivered,
                'updateAt'     => $item->updated_at,
            ];
        });

        return $data->toArray();
    }
}
