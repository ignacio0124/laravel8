<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CarListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->collection->map(function ($item) {
            return [
                'name'         => $item->name,
                'color'        => $item->color_text,
                'displacement' => $item->displacement,
                'price'        => $item->price,
            ];
        });

        return $data->toArray();
    }
}
