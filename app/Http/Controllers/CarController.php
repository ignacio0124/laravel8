<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarListCollection;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Services\CarService;
use App\Http\Requests\SearchRequest;

class CarController extends Controller
{
    protected $car;

    public function __construct(CarService $car)
    {
        $this->car = $car;
    }

    /**
     * 取得車子列表
     *
     * @param SearchRequest $request
     * @return CarListCollection
     */
    public function getList(SearchRequest $request)
    {
        $params = $request->validated();

        $cars = $this->car->getCarList($params);

        return new CarListCollection($cars);
    }
}
