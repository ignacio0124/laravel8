<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CarService;
use App\Http\Resources\UserCarListCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserCarController extends Controller
{
    protected $car;

    public function __construct(CarService $car)
    {
        $this->car = $car;
    }

    /**
     * 取得使用者所有的汽車
     *
     * @param Request $request
     * @return UserCarListCollection
     */
    public function getList(Request $request)
    {
        $userId = $request->userId;

        if (!is_numeric($userId)) {
            throw new NotFoundHttpException(404);
        }

        if (!$this->car->user->exists($userId)) {
            throw new NotFoundHttpException(404);
        }

        $cars = $this->car->userCar->getAllByUserId($userId);

        return new UserCarListCollection($cars);
    }

    /**
     * 取得使用者尚未交付的汽車
     *
     * @param Request $request
     * @return UserCarListCollection
     */
    public function getUnDeliveredList(Request $request)
    {
        $userId = $request->userId;

        if (!is_numeric($userId)) {
            throw new NotFoundHttpException(404);
        }

        if (!$this->car->user->exists($userId)) {
            throw new NotFoundHttpException(404);
        }

        $cars = $this->car->userCar->getUnDeliveredByUserId($userId);

        return new UserCarListCollection($cars);
    }
}
