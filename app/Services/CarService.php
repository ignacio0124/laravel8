<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\CarRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserCarRepository;

class CarService
{
    public $userCar;
    public $user;

    private $car;

    public function __construct(
        UserCarRepository $userCar,
        UserRepository $user,
        CarRepository $car
    ) {
        $this->userCar = $userCar;
        $this->user    = $user;
        $this->car     = $car;
    }

    /**
     * 取得車子列表
     *
     * @param array $data
     * @return Collection
     */
    public function getCarList($data)
    {
        $search = Arr::get($data, 'search', '');

        return $this->car->searchByName($search);
    }
}
