<?php

namespace Tests\Feature\UserCar;

use Illuminate\Database\Eloquent\Factories\Sequence;
use App\Models\User;
use App\Models\UserCar;
use Tests\TestCase;

class ReadTest extends TestCase
{
    public function testGetList()
    {
        $user = User::factory()->create();
        $userCar = UserCar::factory(2)
            ->state(new Sequence(
                ['is_delivered' => false],
                ['is_delivered' => true],
            ))
            ->create([
                'user_id' => $user->id,
            ]);

        $response = $this->getJson('user/' . $user->id . '/cars');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            [
                'color',
                'displacement',
                'price',
                'isDelivered',
                'updateAt',
            ]
        ]);
    }
}
