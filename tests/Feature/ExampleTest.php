<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testFactory()
    {
        $user = User::factory(6)
            ->state(new Sequence(
                ['remember_token' => Str::random(10)],
                ['remember_token' => Str::random(20)],
            ))->create();

        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
