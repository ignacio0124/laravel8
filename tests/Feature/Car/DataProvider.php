<?php

namespace Tests\Feature\Car;

trait DataProvider
{
    public function getUserCarListSearchByDataProvider()
    {
        return [
            [
                ['search' => 'NULL'],
                [
                    'field'    => 'name',
                    'expected' => '',
                    'type'     => 'empty',
                ],
            ],
            [
                ['search' => 'U6'],
                [
                    'field'    => 'name',
                    'expected' => 'U6',
                    'type'     => 'match',
                ],
            ]
        ];
    }
}
