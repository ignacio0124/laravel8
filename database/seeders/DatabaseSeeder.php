<?php

namespace Database\Seeders;

use App\Models\UserCar;
use App\Models\Car;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory(5)->create();
        $cars = Car::factory(5)
            ->state(new Sequence(
                ['color' => Car::COLOR_WHITE],
                ['color' => Car::COLOR_BLUE],
                ['color' => Car::COLOR_RED],
                ['color' => Car::COLOR_BLACK],
            ))
            ->state(new Sequence(
                ['displacement' => 1800],
                ['displacement' => 1600],
                ['displacement' => 2000],
                ['displacement' => 2000],
                ['displacement' => 3000],
            ))
            ->state(new Sequence(
                ['price' => 88],
                ['price' => 76],
                ['price' => 90],
                ['price' => 87],
                ['price' => 135],
            ))
            ->state(new Sequence(
                ['name' => 'AMG'],
                ['name' => 'RAV4'],
                ['name' => 'R8'],
                ['name' => 'U6'],
                ['name' => 'A4'],
            ))
            ->create();
        $car = UserCar::factory(5)
            ->state(new Sequence(
                ['is_delivered' => false],
                ['is_delivered' => true],
            ))
            ->state(new Sequence(
                ['car_id' => $cars[0]->id],
                ['car_id' => $cars[1]->id],
                ['car_id' => $cars[2]->id],
                ['car_id' => $cars[3]->id],
                ['car_id' => $cars[4]->id],
            ))
            ->create([
                'user_id' => $user->first()->id
            ]);
    }
}
