<?php

namespace Database\Factories;

use App\Models\UserCar;
use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'         => $this->faker->word,
            'color'        => $this->faker->numberBetween(0, 3),
            'displacement' => $this->faker->numberBetween(1000, 5000),
            'price'        => $this->faker->numberBetween(50, 3000),
        ];
    }
}
